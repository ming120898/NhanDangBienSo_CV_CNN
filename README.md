- Cài python trên máy tính (xem hướng dẫn trên WEB)
	+ Setup các thư viện cần thiết : Chạy lệnh --> pip install -r requirements.txt
- Model đã được train và lưu trong thư mục model 
- Train lại model :
	+ giải nén tệp train_data/train_data.rar , bổ sung dữ liệu train 
		train_data\
			0\
			1\
			...
			Z\
	+ Chạy lệnh : python train.py --dataset train_data --model model.model --label-bin bin
	+ Chạy lệnh : python Convert_keras_to_tf.py --keras_model model.model --tf_model tf_model.pb
	+ Dự đoán : Chạy file DuDoan_ver2.py thay đổi thư mục chứa ảnh tại dòng 248 và 250 
- Chạy ứng dụng dự đoán
	+ Chuyển đến thư mục app_nhandien
	+ Chạy lệnh : set FLASK_APP=app.py
	+ Chạy lệnh : flask run
	+ Mở đường dẫn được hiển thị
	
- Xem quá trình xử lý ảnh chạy BienSo_Process.ipynb trên jupyter notebook
- Xem demo quá trình xử lý và nhận dạng tại file README.pdf
![alt text](https://github.com/minhphan1208/NhanDangBienSo_CV_CNN/blob/master/DEMO1.png)
![alt text](https://github.com/minhphan1208/NhanDangBienSo_CV_CNN/blob/master/DEMO2.png)
![alt text](https://github.com/minhphan1208/NhanDangBienSo_CV_CNN/blob/master/plot.png)
