# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 16:14:31 2020

@author: MinhPN12
"""
import os
import imutils
import math
import numpy as np
import cv2 as cv

def _drawBoundingBox(img, cnt):
  x,y,w,h = cv.boundingRect(cnt)
  img = cv.rectangle(img, (x,y),(x+w,y+h),(0,255,0),2)
  return img

path = "BienSoXe_ChuaDuocCat"
List_pictures = os.listdir(os.path.expanduser(path))


path_pictures = "BienSoXe_ChuaDuocCat"
for picture in List_pictures:
    path_picture = path_pictures + "//" + picture
    img = cv.imread(path_picture)
    #Danh sách Contours
    Cnts = []
    
    #Tạo image khác để lọc Contourn
    img1 = img.copy()
    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    #Lọc với ngưỡng 170
    ret,img1 = cv.threshold(img1,170,255,cv.THRESH_BINARY)
    contours, hierarchy = cv.findContours(img1, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    
    for cnt in contours:
        x,y,w,h = cv.boundingRect(cnt)
        rect_area = w*h
        if w > 400 and h > 200 and w*h > 10000 and x >0 :
            Cnts.append(cnt)
            
    img1 = img.copy()
    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    #Lọc với ngưỡng 110
    ret,img1 = cv.threshold(img1,110,255,cv.THRESH_BINARY)
    contours, hierarchy = cv.findContours(img1, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x,y,w,h = cv.boundingRect(cnt)
        rect_area = w*h
        if w > 400 and h > 200 and w*h > 10000 and x >0 :
            Cnts.append(cnt)
            
            
    img1 = img.copy()
    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    #Lọc với ngưỡng 180
    ret,img1 = cv.threshold(img1,180,255,cv.THRESH_BINARY)
    contours, hierarchy = cv.findContours(img1, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x,y,w,h = cv.boundingRect(cnt)
        rect_area = w*h
        if w > 400 and h > 200 and w*h > 10000 and x >0 :
            Cnts.append(cnt)

    #Tạo image khác để lọc theo hệ màu HSV    
    imgCopy = img.copy()
    hsv_img = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    
    #Ngưỡng thấp nhất và cao nhất của màu trắng
    lower_WHITE = np.array([0, 0, 150])
    upper_WHITE = np.array([200, 255, 255])
    masking = cv.inRange(hsv_img, lower_WHITE, upper_WHITE)
    
    kernel_erode = np.ones((3,3), np.uint8)
    eroded_mask = cv.erode(masking, kernel_erode, iterations=1)
    kernel_dilate = np.ones((3,3),np.uint8)
    dilated_mask = cv.dilate(eroded_mask, kernel_dilate, iterations=1)
    #Lọc Contour
    cnts = cv.findContours(dilated_mask, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    
    
    #Chiều cao , chiều rộng và số chiểu của image
    cc, cr,c = img.shape
    #Lọc tất cả các Contour từ các ngưỡng khác nhau
    for contour in cnts:  
        x, y, w, h = cv.boundingRect(contour)
        rect_area = w*h
        if 2500> w > 300 and 1000 > h > 200 and 1.2< w/h < 1.9:
            Cnts.append(contour)
    if( len(Cnts) > 0 ):
        #Biến lưu khoảng cách đến tâm image
        c_d = []
        for cnt in Cnts :
            M = cv.moments(cnt)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            c_d.append(math.sqrt((cx-cr/2)**2 + (cy-cc/2)**2))
        c_d.sort()
        for cnt in Cnts :
            M = cv.moments(cnt)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            if(math.sqrt((cx-cr/2)**2 + (cy-cc/2)**2) == c_d[0]):
                imgCopy = _drawBoundingBox(imgCopy,cnt)
                x, y, w, h = cv.boundingRect(cnt)
                print( x, y, w, h)
                imgCopy = img[y:y+h, x:x+w]
                path_save = "BienSoXe_DaDuocCat/" + picture
                cv.imwrite(path_save, imgCopy)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    