import os
import imutils
import math
import numpy as np
import cv2 as cv
import tensorflow as tf
from tensorflow.python.platform import gfile
import cv2
import numpy as np
import time
import glob
import os
import time

start_time = time.time()

def _drawBoundingBox(img, cnt):
  x,y,w,h = cv.boundingRect(cnt)
  img = cv.rectangle(img, (x,y),(x+w,y+h),(0,255,0),2)
  return img

def non_max_suppression(boxes, overlapThresh):
  # Nếu không có bounding boxes thì trả về empty list
  if len(boxes)==0:
    return []
  # Nếu bounding boxes nguyên thì chuyển sang float.
  if boxes.dtype.kind == "i":
    boxes = boxes.astype("float")
  # Khởi tạo list của index được lựa chọn
  pick = []
  # Lấy ra tọa độ của các bounding boxes
  x1 = boxes[:,0]
  y1 = boxes[:,1]
  x2 = boxes[:,2]
  y2 = boxes[:,3]
  # Tính toàn diện tích của các bounding boxes và sắp xếp chúng theo thứ tự từ bottom-right, chính là tọa độ theo y của bounding box
  area = (x2 - x1 + 1) * (y2 - y1 + 1)
  idxs = np.argsort(y2)
  # Khởi tạo một vòng while loop qua các index xuất hiện trong indexes
  while len(idxs) > 0:
    # Lấy ra index cuối cùng của list các indexes và thêm giá trị index vào danh sách các indexes được lựa chọn
    last = len(idxs) - 1
    i = idxs[last]
    pick.append(i)
    # Tìm cặp tọa độ lớn nhất (x, y) là điểm bắt đầu của bounding box và tọa độ nhỏ nhất (x, y) là điểm kết thúc của bounding box
    xx1 = np.maximum(x1[i], x1[idxs[:last]])
    yy1 = np.maximum(y1[i], y1[idxs[:last]])
    xx2 = np.minimum(x2[i], x2[idxs[:last]])
    yy2 = np.minimum(y2[i], y2[idxs[:last]])
    # Tính toán width và height của bounding box
    w = np.maximum(0, xx2 - xx1 + 1)
    h = np.maximum(0, yy2 - yy1 + 1)
    # Tính toán tỷ lệ diện tích overlap
    overlap = (w * h) / area[idxs[:last]]
    # Xóa index cuối cùng và index của bounding box mà tỷ lệ diện tích overlap > overlapThreshold
    idxs = np.delete(idxs, np.concatenate(([last],
      np.where(overlap > overlapThresh)[0])))
  # Trả ra list các index được lựa chọn
  return boxes[pick].astype("int")


#"""------------------------------Hàm cắt biển số------------------------------""""
def CatBien(path_picture):
    img = cv.imread(path_picture)
    #Danh sách Contours
    Cnts = []
    
    #Tạo image khác để lọc Contourn
    img1 = img.copy()
    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    #Lọc với ngưỡng 170
    ret,img1 = cv.threshold(img1,170,255,cv.THRESH_BINARY)
    contours, hierarchy = cv.findContours(img1, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    
    for cnt in contours:
        x,y,w,h = cv.boundingRect(cnt)
        rect_area = w*h
        if w > 400 and h > 200 and w*h > 10000 and x >0 :
            Cnts.append(cnt)
            
    img1 = img.copy()
    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    #Lọc với ngưỡng 110
    ret,img1 = cv.threshold(img1,110,255,cv.THRESH_BINARY)
    contours, hierarchy = cv.findContours(img1, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x,y,w,h = cv.boundingRect(cnt)
        rect_area = w*h
        if w > 400 and h > 200 and w*h > 10000 and x >0 :
            Cnts.append(cnt)
            
            
    img1 = img.copy()
    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    #Lọc với ngưỡng 180
    ret,img1 = cv.threshold(img1,180,255,cv.THRESH_BINARY)
    contours, hierarchy = cv.findContours(img1, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x,y,w,h = cv.boundingRect(cnt)
        if w > 400 and h > 200 and w*h > 10000 and x >0 :
            Cnts.append(cnt)

    #Tạo image khác để lọc theo hệ màu HSV    
    imgCopy = img.copy()
    hsv_img = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    
    #Ngưỡng thấp nhất và cao nhất của màu trắng
    lower_WHITE = np.array([0, 0, 150])
    upper_WHITE = np.array([200, 255, 255])
    masking = cv.inRange(hsv_img, lower_WHITE, upper_WHITE)
    
    kernel_erode = np.ones((3,3), np.uint8)
    eroded_mask = cv.erode(masking, kernel_erode, iterations=1)
    kernel_dilate = np.ones((3,3),np.uint8)
    dilated_mask = cv.dilate(eroded_mask, kernel_dilate, iterations=1)
    #Lọc Contour
    cnts = cv.findContours(dilated_mask, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    
    
    #Chiều cao , chiều rộng và số chiểu của image
    cc, cr,c = img.shape
    #Lọc tất cả các Contour từ các ngưỡng khác nhau
    for contour in cnts:  
        x, y, w, h = cv.boundingRect(contour)
        if 2500> w > 300 and 1000 > h > 200 and 1.2< w/h < 1.9:
            Cnts.append(contour)
    if( len(Cnts) > 0 ):
        #Biến lưu khoảng cách đến tâm image
        c_d = []
        for cnt in Cnts :
            M = cv.moments(cnt)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            c_d.append(math.sqrt((cx-cr/2)**2 + (cy-cc/2)**2))
            #imgCopy = _drawBoundingBox(imgCopy,cnt)
        c_d.sort()
        for cnt in Cnts :
            M = cv.moments(cnt)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            if(math.sqrt((cx-cr/2)**2 + (cy-cc/2)**2) == c_d[0]):
                imgCopy = _drawBoundingBox(imgCopy,cnt)
                x, y, w, h = cv.boundingRect(cnt)
                #print( x, y, w, h)
                imgCopy = img[y:y+h, x:x+w]
    return imgCopy


#"""------------------------------Hàm cắt từng kí tự từ biển số------------------------------""""
def CatTungSo(imgBienSo):
    img = imgBienSo
    img = cv.resize(img, (300, 150))
    imgGray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    kernel = np.ones((2,2),np.float32)/4
    imgBi = cv.filter2D(imgGray,-1,kernel)
    imgBi = cv.filter2D(imgBi,-1,kernel)
    imgBi = cv.adaptiveThreshold(imgBi,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,\
               cv.THRESH_BINARY,11,2)
    cnts = cv.findContours(imgBi, cv.RETR_LIST,
    	cv.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    digitCnts = []
    for contour in cnts:
        x, y, w, h  = cv.boundingRect(contour)
        rect_area = w*h
        if w > 20 and h > 40 and cv.contourArea(contour)/rect_area > 0.3 and  h/w > 1 and h <= 80:
            digitCnts.append(contour)
    
    boundingBoxes = []
    for i in range(len(digitCnts)):
      cnt = digitCnts[i]
      x,y,w,h = cv.boundingRect(cnt)
      x1, y1, x2, y2 = x, y, x+w, y+h
      boundingBoxes.append((x1, y1, x2, y2))
    boundingBoxes = [box for box in boundingBoxes if box[:2] != (0, 0)]
    boundingBoxes = np.array(boundingBoxes)
    pick = non_max_suppression(boundingBoxes, 0.9)
    imgCopy = img.copy()
    digitCnts.clear()
    for (startX, startY, endX, endY) in pick:
        a = [endX,startX , startY , endY]
        digitCnts.append(a)
        
    kichthuoc_anh = imgCopy.shape
    chieucao = kichthuoc_anh[0]/2
    imgCopy = cv.resize(imgCopy, (148, 98))
    imgCopy = cv.cvtColor(imgCopy, cv.COLOR_BGR2GRAY)
    imgCopy = cv.adaptiveThreshold(imgCopy,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv.THRESH_BINARY,11,2)
    imgCopy = cv.resize(imgCopy, (300, 150))
    digitCnts.sort()
    List = []
    if(len(digitCnts) > 0):
        for a in digitCnts:
            if(a[2] + 5 < chieucao):
                b = imgCopy[a[2]:a[3],a[1]:a[0]]       
                ret,b= cv.threshold(b,127,255,cv.THRESH_BINARY_INV)
                b = cv.resize(b, (32, 32))
                b = np.reshape(b, (32, 32, 1))
                List.append(b)
                
        for a in digitCnts:
            if(a[2] + 5 >= chieucao):
                b = imgCopy[a[2]:a[3],a[1]:a[0]]
                ret,b= cv.threshold(b,127,255,cv.THRESH_BINARY_INV)
                b = cv.resize(b, (32, 32))
                b = np.reshape(b, (32, 32, 1))
                List.append(b)
    return List
         
#"""------------------------------Hàm dự đoán nhận input là 1 ảnh của 1 kí tự------------------------------""""
def DuDoan(img_digit):
    image = img_digit
    image = cv2.resize(image, dsize=(32, 32), interpolation = cv2.INTER_CUBIC)
    image = np.reshape(image, (32, 32, 1))
    #print(image.shape)

    np_image_data = np.asarray(image)
    #np_image_data = cv2.normalize(np_image_data.astype('float'), None, -0.5, .5, cv2.NORM_MINMAX)
    image_tensor = np.expand_dims(np_image_data,axis=0)

    #feed tensor into the network
    softmax_tensor = sess.graph.get_tensor_by_name('import/dense_2/Softmax:0')
    predictions = sess.run(softmax_tensor, {'import/conv2d_1_input:0': image_tensor})
    predictions = np.squeeze(predictions)   
    top_k = predictions.argsort()[-1:][::-1]
    return labels[top_k[0]]


    
#LOAD LABLES
labels = []
proto_as_ascii_lines = tf.gfile.GFile("labels.txt").readlines()
for l in proto_as_ascii_lines:
    labels.append(l.rstrip())

#LOAD MODEL
graph = tf.Graph()
graph_def = tf.GraphDef()
with open("model/tf_model.pb", 'rb') as f:
    graph_def.ParseFromString(f.read())
with graph.as_default():
    tf.import_graph_def(graph_def)
sess = tf.Session(graph=graph) 
 
Count_true = 0
path = "BienSoXe_ChuaDuocCat"
List_pictures = os.listdir(os.path.expanduser(path))
path_pictures = "BienSoXe_ChuaDuocCat"
for picture in List_pictures:
    path_picture = path_pictures + "//" + picture
    img = cv.imread(path_picture)
    img_BienSo = CatBien((path_picture))
    List_digit  = CatTungSo(img_BienSo)
    result = ""
    for i in range(len(List_digit)):
        result +=DuDoan(List_digit[i])
    if(picture.split('.')[0] == result):
        Count_true +=1    
        print("Hình ảnh từ file " + picture + ": -->Kết quả dự đoán : " + result)
print(Count_true)
print("Accuracy : " + str(  round(Count_true/len(List_pictures), 3)*100  )  )
print("-------------------------------------------------------------------")

end_time = time.time()
print('Tổng thời gian Train : %f ms' % ((end_time - start_time) * 1000))








