from flask import Flask
from PIL import Image
import os, os.path
import process
UPLOAD_FOLDER = 'static/uploads/'

app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

import os
from app import app
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/")
def upload_form():
    return render_template('upload.html')
@app.route("/", methods=['POST'])
def upload_image():
    if 'file' not in request.files:
        flash("Chưa chọn file")
        return redirect(request.url)
    file = request.files['file']
    if file.filename =="":
        flash("Không có image nào được upload !")
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
        KQ = process.Ketqua(os.path.join(app.config['UPLOAD_FOLDER'],filename))
        flash("Kết quả dự đoán : "+KQ)
        return render_template("upload.html", filename=filename)
    else:
        flash("Loại file này không cho phép !")
        return redirect(request.url)
    
@app.route('/display/<filename>')
def display_image(filename):
    return redirect(url_for('static',filename='uploads/'+filename), code=301)


@app.route('/xuly/<filename>')
def process_image(filename):
    filename = filename.split(".")[0]
    return render_template("process.html",filename=filename)
    
if __name__ ==  "__main__":
    app.run()
    
    
    
    
    
    
    
    
    
    
    