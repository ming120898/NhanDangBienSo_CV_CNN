import os
import imutils
import math
import numpy as np
import cv2 as cv
import tensorflow as tf
from tensorflow.python.platform import gfile
import cv2

import os
import matplotlib.pyplot as plt


def non_max_suppression(boxes, overlapThresh):
    # Nếu không có bounding boxes thì trả về empty list
    if len(boxes)==0:
        return []
    # Nếu bounding boxes nguyên thì chuyển sang float.
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")
    # Khởi tạo list của index được lựa chọn
    pick = []
    # Lấy ra tọa độ của các bounding boxes
    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]
    # Tính toàn diện tích của các bounding boxes và sắp xếp chúng theo thứ tự từ bo
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
    # Khởi tạo một vòng while loop qua các index xuất hiện trong indexes
    while len(idxs) > 0:
        # Lấy ra index cuối cùng của list các indexes và thêm giá trị index vào danh
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        # Tìm cặp tọa độ lớn nhất (x, y) là điểm bắt đầu của bounding box và tọa độ n
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        # Tính toán width và height của bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        # Tính toán tỷ lệ diện tích overlap
        overlap = (w * h) / area[idxs[:last]]
        # Xóa index cuối cùng và index của bounding box mà tỷ lệ diện tích overlap >
        idxs = np.delete(idxs, np.concatenate(([last],np.where(overlap > overlapThresh)[0])))
    # Trả ra list các index được lựa chọn
    return boxes[pick].astype("int")

#Hàm cắt Contour
def _drawBoundingBox(img, cnt):
    x,y,w,h = cv.boundingRect(cnt)
    img = cv.rectangle(img, (x,y),(x+w,y+h),(0,255,0),3)
    return img

#Hàm vẽ các Contour trên ảnh
def drawBox(img,Cnts):
    for i in range(len(Cnts)):
        x,y,w,h = cv.boundingRect(Cnts[i])
        img = cv.rectangle(img, (x,y),(x+w,y+h),(0,255,0),3)
    return img
    
#Hàm dự đoán kí tự
def DuDoan(img_digit):
    image = img_digit
    image = cv2.resize(image, dsize=(32, 32), interpolation = cv2.INTER_CUBIC)
    image = np.reshape(image, (32, 32, 1))
    #print(image.shape)

    np_image_data = np.asarray(image)
    #np_image_data = cv2.normalize(np_image_data.astype('float'), None, -0.5, .5, cv2.NORM_MINMAX)
    image_tensor = np.expand_dims(np_image_data,axis=0)

    #feed tensor into the network
    softmax_tensor = sess.graph.get_tensor_by_name('import/dense_2/Softmax:0')
    predictions = sess.run(softmax_tensor, {'import/conv2d_1_input:0': image_tensor})
    predictions = np.squeeze(predictions)   
    top_k = predictions.argsort()[-1:][::-1]
    return labels[top_k[0]]

#Load Label và model đã train
labels = []
proto_as_ascii_lines = tf.gfile.GFile("labels.txt").readlines()
for l in proto_as_ascii_lines:
    labels.append(l.rstrip())

#LOAD MODEL
graph = tf.Graph()
graph_def = tf.GraphDef()
with open("model/tf_model.pb", 'rb') as f:
    graph_def.ParseFromString(f.read())
with graph.as_default():
    tf.import_graph_def(graph_def)
sess = tf.Session(graph=graph) 

def Ketqua(path_img):
    path_save_process =  "static/process/"
        
    img = cv.imread(path_img)
    filename = path_img.split("/")[2].split(".")[0]
    if(str(os.path.isdir("static/process/" +  filename)) == "False"):
        os.mkdir("static/process/" +  filename)
    cv.imwrite(path_save_process+"/"+filename+"/"+"1.jpg", img)
    #Biến lưu danh sách tất cả Contour
    Cnts = []
    #Tạo image khác để lọc Contour
    imgCopy = img.copy()
    #Chuyển sang ảnh xám để lọc
    imgCopy = cv.cvtColor(imgCopy, cv.COLOR_BGR2GRAY)
    cv.imwrite(path_save_process+"/"+filename+"/"+"2.jpg", imgCopy)
    #Phân ngưỡng với ngưỡng 170
    ret,imgCopy = cv.threshold(imgCopy,170,255,cv.THRESH_BINARY)
    cv.imwrite(path_save_process+"/"+filename+"/"+"3.jpg", imgCopy)
    #Tìm tất cả các Contour với ngưỡng 170
    imgCopyCnts = img.copy()
    contours, hierarchy = cv.findContours(imgCopy, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    cv.imwrite(path_save_process+"/"+filename+"/"+"4.jpg", drawBox(imgCopyCnts,contours))
    #Lọc với điều kiện và lưu vào biến Cnts
    for cnt in contours:
            x,y,w,h = cv.boundingRect(cnt)
            rect_area = w*h
            if w > 400 and h > 200 and w*h > 10000 and x >0 :
                Cnts.append(cnt)
    imgCopyCnts = img.copy()
    cv.imwrite(path_save_process+"/"+filename+"/"+"5.jpg", drawBox(imgCopyCnts,Cnts))
    #Tạo ảnh mới để tiếp tục lọc
    imgCopy = img.copy()
    imgCopy = cv.cvtColor(imgCopy, cv.COLOR_BGR2GRAY)
    #Lọc với ngưỡng 110
    ret,imgCopy = cv.threshold(imgCopy,110,255,cv.THRESH_BINARY)
    cv.imwrite(path_save_process+"/"+filename+"/"+"6.jpg", imgCopy)
    #Tìm Contour với ngưỡng 110
    imgCopyCnts = img.copy()
    contours, hierarchy = cv.findContours(imgCopy, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    cv.imwrite(path_save_process+"/"+filename+"/"+"7.jpg",drawBox(imgCopyCnts,contours))
    #Lọc Contour với ngưỡng 110 và lưu vào biến Cnts
    for cnt in contours:
            x,y,w,h = cv.boundingRect(cnt)
            rect_area = w*h
            if w > 400 and h > 200 and w*h > 10000 and x >0 :
                Cnts.append(cnt)
    imgCopyCnts = img.copy()
    cv.imwrite(path_save_process+"/"+filename+"/"+"8.jpg",drawBox(imgCopyCnts,Cnts))
    #Phân ngưỡng với ngưỡng 180
    imgCopy = img.copy()
    imgCopy = cv.cvtColor(imgCopy, cv.COLOR_BGR2GRAY)
    ret,imgCopy = cv.threshold(imgCopy,180,255,cv.THRESH_BINARY)
    cv.imwrite(path_save_process+"/"+filename+"/"+"9.jpg",imgCopy)
    #Tìm Contour với ngưỡng 180
    imgCopyCnts = img.copy()
    contours, hierarchy = cv.findContours(imgCopy, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    cv.imwrite(path_save_process+"/"+filename+"/"+"10.jpg",drawBox(imgCopyCnts,contours))
    #Lọc Contour với ngưỡng 180
    for cnt in contours:
            x,y,w,h = cv.boundingRect(cnt)
            rect_area = w*h
            if w > 400 and h > 200 and w*h > 10000 and x > 0 :
                Cnts.append(cnt)
    imgCopyCnts = img.copy()
    cv.imwrite(path_save_process+"/"+filename+"/"+"11.jpg",drawBox(imgCopyCnts,Cnts))
    #Tạo image khác để lọc theo hệ màu HSV    
    imgCopy = img.copy()
    hsv_img = cv.cvtColor(imgCopy, cv.COLOR_BGR2HSV)
    cv.imwrite(path_save_process+"/"+filename+"/"+"12.jpg",hsv_img)
    #Ngưỡng thấp nhất và cao nhất của màu trắng
    lower_WHITE = np.array([0, 0, 150])
    upper_WHITE = np.array([200, 255, 255])
    masking = cv.inRange(hsv_img, lower_WHITE, upper_WHITE)
    cv.imwrite(path_save_process+"/"+filename+"/"+"13.jpg",masking)
    kernel_erode = np.ones((3,3), np.uint8)
    eroded_mask = cv.erode(masking, kernel_erode, iterations=1)
    kernel_dilate = np.ones((3,3),np.uint8)
    dilated_mask = cv.dilate(eroded_mask, kernel_dilate, iterations=1)
    cv.imwrite(path_save_process+"/"+filename+"/"+"14.jpg",dilated_mask)
    #Tìm Contour với ảnh HSV
    imgCopyCnts = img.copy()
    cnts = cv.findContours(dilated_mask, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cv.imwrite(path_save_process+"/"+filename+"/"+"15.jpg",drawBox(imgCopyCnts,cnts))
    #Lọc Contour với hệ màu HSV
    for contour in cnts:  
        x, y, w, h = cv.boundingRect(contour)
        if 2500> w > 300 and 1000 > h > 200 and 1.2< w/h < 1.9:
            Cnts.append(contour)
    imgCopyCnts = img.copy()
    cv.imwrite(path_save_process+"/"+filename+"/"+"16.jpg",drawBox(imgCopyCnts,Cnts))
    ImgBienSo = ""
    #Chiều cao , chiều rộng và số chiểu của image
    cc, cr,c = img.shape
    #Lọc tất cả các Contour từ các ngưỡng khác nhau
    for contour in cnts:  
        x, y, w, h = cv.boundingRect(contour)
        if 2500> w > 300 and 1000 > h > 200 and 1.2< w/h < 1.9:
            Cnts.append(contour)
    if( len(Cnts) > 0 ):
        #Biến lưu khoảng cách đến tâm image
        c_d = []
        for cnt in Cnts :
            M = cv.moments(cnt)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            c_d.append(math.sqrt((cx-cr/2)**2 + (cy-cc/2)**2))
            #imgCopy = _drawBoundingBox(imgCopy,cnt)
        c_d.sort()
        for cnt in Cnts :
            M = cv.moments(cnt)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            if(math.sqrt((cx-cr/2)**2 + (cy-cc/2)**2) == c_d[0]):
                imgCopy = _drawBoundingBox(imgCopy,cnt)
                x, y, w, h = cv.boundingRect(cnt)
                #print( x, y, w, h)
                imgCopy = img[y:y+h, x:x+w]
        ImgBienSo = imgCopy
        cv.imwrite(path_save_process+"/"+filename+"/"+"17.jpg",imgCopy)
    ImgBienSo = cv.resize(ImgBienSo, (300, 150))
    imgGray = cv.cvtColor(ImgBienSo, cv.COLOR_BGR2GRAY)
    kernel = np.ones((2,2),np.float32)/4
    imgBi = cv.filter2D(imgGray,-1,kernel)
    imgBi = cv.filter2D(imgBi,-1,kernel)
    cv.imwrite(path_save_process+"/"+filename+"/"+"18.jpg",imgBi)
    imgBi = cv.adaptiveThreshold(imgBi,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,\
                   cv.THRESH_BINARY,11,2)
        
    cv.imwrite(path_save_process+"/"+filename+"/"+"19.jpg",imgBi)    
    #ImgBienSo = cv.resize(ImgBienSo, (300, 150))
    imgCopyCnts = ImgBienSo.copy()
    cnts = cv.findContours(imgBi, cv.RETR_LIST,cv.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cv.imwrite(path_save_process+"/"+filename+"/"+"20.jpg",drawBox(imgCopyCnts,cnts))
    
    digitCnts = []
    for contour in cnts:
        x, y, w, h  = cv.boundingRect(contour)
        rect_area = w*h
        if w > 20 and h > 40 and cv.contourArea(contour)/rect_area > 0.3 and  h/w > 1 and h <= 80:
            digitCnts.append(contour)
    
    #ImgBienSo = cv.resize(ImgBienSo, (300, 150))
    imgCopyCnts = ImgBienSo.copy()
    cv.imwrite(path_save_process+"/"+filename+"/"+"21.jpg",drawBox(imgCopyCnts,digitCnts))
    boundingBoxes = []
    for i in range(len(digitCnts)):
        cnt = digitCnts[i]
        x,y,w,h = cv.boundingRect(cnt)
        x1, y1, x2, y2 = x, y, x+w, y+h
        boundingBoxes.append((x1, y1, x2, y2))
    boundingBoxes = [box for box in boundingBoxes if box[:2] != (0, 0)]
    boundingBoxes = np.array(boundingBoxes)
    pick = non_max_suppression(boundingBoxes, 0.9)
    imgCopy = ImgBienSo.copy()
    digitCnts.clear()
    for (startX, startY, endX, endY) in pick:
        a = [endX,startX , startY , endY]
        digitCnts.append(a)
        
    #Tìm các kí tự và sắp xếp theo đúng thứ tự
    kichthuoc_anh = imgCopy.shape
    chieucao = kichthuoc_anh[0]/2
    imgCopy = cv.resize(imgCopy, (148, 98))
    imgCopy = cv.cvtColor(imgCopy, cv.COLOR_BGR2GRAY)
    imgCopy = cv.adaptiveThreshold(imgCopy,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv.THRESH_BINARY,11,2)
    imgCopy = cv.resize(imgCopy, (300, 150))
    digitCnts.sort()
    List = []
    if(len(digitCnts) > 0):
        for a in digitCnts:
            if(a[2] + 5 < chieucao):
                b = imgCopy[a[2]:a[3],a[1]:a[0]]       
                ret,b= cv.threshold(b,127,255,cv.THRESH_BINARY_INV)
                b = cv.resize(b, (32, 32))
                b = np.reshape(b, (32, 32, 1))
                List.append(b)
                    
        for a in digitCnts:
            if(a[2] + 5 >= chieucao):
                b = imgCopy[a[2]:a[3],a[1]:a[0]]
                ret,b= cv.threshold(b,127,255,cv.THRESH_BINARY_INV)
                b = cv.resize(b, (32, 32))
                b = np.reshape(b, (32, 32, 1))
                List.append(b)   
    #Lưu ảnh vào 1 thư mục
    i = 1
    for digit in List:
        path_save =  "KiTu/"+ str(i) + ".jpg"
        cv.imwrite(path_save, digit)
        i+=1         
    #Dự đoán kết quả
    List_digit = os.listdir(os.path.expanduser("KiTu"))
    i = 0
    result = ""
    for path in List_digit:
        a = cv.imread("KiTu/" +path )
        if(i < len(List)):
            result +=DuDoan(List[i])
            i+=1  
    #Kết quả dự đoán
    return result  
































