$(document).ready(function() {
    $.ajax({
        type: POST_TYPE,
        url: "/filter_doctor",
        data: get_filter_data(),
        dataType: DATA_TYPE,
        success: function(data) {
            $("#loadMe").modal("hide");
            setTimeout(function() {
                main_table.setData(data[KEY_SEARCH_DATA]);
                var a = main_table.getDataCount();
                var total_result = document.getElementById("total_result");
                total_result.innerHTML = a;
                // $("#dowload-file").attr(HREF, data[KEY_EXCEL_LINK]);
            }, 100);
        },
        error: function(data1) {
            $("#loadMe").modal("hide");
            show_alert(data1)
        }
    })
})