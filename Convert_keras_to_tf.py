from keras import backend as K
import tensorflow as tf
from keras.models import load_model
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--keras_model", required=True,
    help="path to input keras model")
ap.add_argument("-m", "--tf_model", required=True,
    help="path to output tf model")
args = vars(ap.parse_args())

K.set_learning_phase(0)
model = load_model(args["keras_model"])

print(model.outputs)
print(model.inputs)

def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    from tensorflow.python.framework.graph_util import convert_variables_to_constants
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        # Graph -> GraphDef ProtoBuf
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = convert_variables_to_constants(session, input_graph_def,output_names, freeze_var_names)
        return frozen_graph
frozen_graph = freeze_session(K.get_session(),
                              output_names=[out.op.name for out in model.outputs])
                              
tf.train.write_graph(frozen_graph, "model", args["tf_model"], as_text=False)