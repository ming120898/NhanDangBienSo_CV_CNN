#import thư viện cần thiết
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers import Dense, Dropout, Activation, Flatten
import keras
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import random
import pickle
import cv2
import os


data = []
labels = []
num_classes = 31
batch_size = 32
EPOCHS = 50
INIT_LR = 0.0001

#Khai báo tham số
#python train.py --dataset train_data --model model.model --label-bin bin
#python Convert_keras_to_tf.py --keras_model model.model --tf_model tf_model.pb
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
    help="path to input dataset of images")
ap.add_argument("-m", "--model", required=True,
    help="path to output trained model")
ap.add_argument("-l", "--label-bin", required=True,
    help="path to output label binarizer")
args = vars(ap.parse_args())


#Trộn ảnh
imagePaths = sorted(list(paths.list_images(args["dataset"])))
random.seed(42)
random.shuffle(imagePaths)

#Load ảnh từ thư mục train_data
for imagePath in imagePaths:
    image = cv2.imread(imagePath,0)
    image = cv2.resize(image, (32, 32))
    image = np.reshape(image, (32, 32, 1))
    data.append(image)
    label = imagePath.split(os.path.sep)[-2]
    labels.append(label)
    
#Chuẩn hóa ảnh về [0,1]
data = np.array(data, dtype="float") / 255.0

#Chia tập train 80% và test 20%
(trainX, testX, trainY, testY) = train_test_split(data,
    labels, test_size=0.2, random_state=42)

#One-hot label
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)

#Xây dựng mạng
model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=(32,32,1)))
model.add(Activation('relu'))
model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(128))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes,activation = 'softmax'))
model.summary()

#Compiling the CNN
opt = keras.optimizers.rmsprop(lr=INIT_LR, decay=1e-6)
model.compile(optimizer = opt, loss = 'categorical_crossentropy', metrics = ['accuracy'])
H = model.fit(trainX, trainY, validation_data=(testX, testY), epochs=EPOCHS, batch_size = batch_size)

# evaluate the network
print("[INFO] evaluating network...")
predictions = model.predict(testX, batch_size= batch_size)
print(classification_report(testY.argmax(axis=1),
    predictions.argmax(axis=1), target_names=lb.classes_))

#Vẽ đồ thị cho acc và loss
N = np.arange(0, EPOCHS)
plt.style.use("ggplot")
plt.figure()
plt.plot(N, H.history["loss"], label="train_loss")
plt.plot(N, H.history["val_loss"], label="val_loss")
plt.plot(N, H.history["acc"], label="train_acc")
plt.plot(N, H.history["val_acc"], label="val_acc")
plt.title("Quá trình Training")
plt.xlabel("Epoch")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig("Training_plot")

#Lưu label 
print("[INFO] serializing network and label binarizer...")
model.save(args["model"])
f = open(args["label_bin"], "wb")
f.write(pickle.dumps(lb))
f.close()



