# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 19:38:21 2020

@author: ADMIN
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 21:24:10 2020

@author: ADMIN
"""


import cv2
import numpy as np
from matplotlib import pyplot as plt
from imutils import contours
import imutils
import os

def non_max_suppression(boxes, overlapThresh):
  # Nếu không có bounding boxes thì trả về empty list
  if len(boxes)==0:
    return []
  # Nếu bounding boxes nguyên thì chuyển sang float.
  if boxes.dtype.kind == "i":
    boxes = boxes.astype("float")
  # Khởi tạo list của index được lựa chọn
  pick = []
  # Lấy ra tọa độ của các bounding boxes
  x1 = boxes[:,0]
  y1 = boxes[:,1]
  x2 = boxes[:,2]
  y2 = boxes[:,3]
  # Tính toàn diện tích của các bounding boxes và sắp xếp chúng theo thứ tự từ bottom-right, chính là tọa độ theo y của bounding box
  area = (x2 - x1 + 1) * (y2 - y1 + 1)
  idxs = np.argsort(y2)
  # Khởi tạo một vòng while loop qua các index xuất hiện trong indexes
  while len(idxs) > 0:
    # Lấy ra index cuối cùng của list các indexes và thêm giá trị index vào danh sách các indexes được lựa chọn
    last = len(idxs) - 1
    i = idxs[last]
    pick.append(i)
    # Tìm cặp tọa độ lớn nhất (x, y) là điểm bắt đầu của bounding box và tọa độ nhỏ nhất (x, y) là điểm kết thúc của bounding box
    xx1 = np.maximum(x1[i], x1[idxs[:last]])
    yy1 = np.maximum(y1[i], y1[idxs[:last]])
    xx2 = np.minimum(x2[i], x2[idxs[:last]])
    yy2 = np.minimum(y2[i], y2[idxs[:last]])
    # Tính toán width và height của bounding box
    w = np.maximum(0, xx2 - xx1 + 1)
    h = np.maximum(0, yy2 - yy1 + 1)
    # Tính toán tỷ lệ diện tích overlap
    overlap = (w * h) / area[idxs[:last]]
    # Xóa index cuối cùng và index của bounding box mà tỷ lệ diện tích overlap > overlapThreshold
    idxs = np.delete(idxs, np.concatenate(([last],
      np.where(overlap > overlapThresh)[0])))
  # Trả ra list các index được lựa chọn
  return boxes[pick].astype("int")


path1 = "BienSoXe_DaDuocCat"
#Lấy danh sách file có trong thư mục chứa ảnh biển số
List_pictures = os.listdir(os.path.expanduser(path1))
print(List_pictures)
List_pictures.reverse()
for path in List_pictures:
    b = path1 + "/" + str(path)
    img = cv2.imread(b)
    img = cv2.resize(img, (300, 150))
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kernel = np.ones((2,2),np.float32)/4
    imgBi = cv2.filter2D(imgGray,-1,kernel)
    imgBi = cv2.filter2D(imgBi,-1,kernel)
    imgBi = cv2.adaptiveThreshold(imgBi,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
               cv2.THRESH_BINARY,11,2)
    cnts = cv2.findContours(imgBi, cv2.RETR_LIST,
    	cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    digitCnts = []
    for contour in cnts:
        x, y, w, h = contourRect = cv2.boundingRect(contour)
        rect_area = w*h
        if w > 20 and h > 40 and cv2.contourArea(contour)/rect_area > 0.3 and  h/w > 1 and h <= 80:
            digitCnts.append(contour)
    
    boundingBoxes = []
    for i in range(len(digitCnts)):
      cnt = digitCnts[i]
      x,y,w,h = cv2.boundingRect(cnt)
      x1, y1, x2, y2 = x, y, x+w, y+h
      boundingBoxes.append((x1, y1, x2, y2))
    boundingBoxes = [box for box in boundingBoxes if box[:2] != (0, 0)]
    boundingBoxes = np.array(boundingBoxes)
    pick = non_max_suppression(boundingBoxes, 0.9)
    imgCopy = img.copy()
    digitCnts.clear()
    for (startX, startY, endX, endY) in pick:
        a = [endX,startX , startY , endY]
        digitCnts.append(a)
        
    kichthuoc_anh = imgCopy.shape
    chieucao = kichthuoc_anh[0]/2
    imgCopy = cv2.resize(imgCopy, (148, 98))
    imgCopy = cv2.cvtColor(imgCopy, cv2.COLOR_BGR2GRAY)
    imgCopy = cv2.adaptiveThreshold(imgCopy,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,11,2)
    imgCopy = cv2.resize(imgCopy, (300, 150))
    i = 0
    digitCnts.sort()
    if(len(digitCnts) > 0):
        os.mkdir("BienSoXe_CatThanhTungKiTu/" +  path.split(".")[0])
        for a in digitCnts:
            if(a[2] + 5 < chieucao):
                b = imgCopy[a[2]:a[3],a[1]:a[0]]       
                ret,b= cv2.threshold(b,127,255,cv2.THRESH_BINARY_INV)
                b = cv2.resize(b, (32, 32))
                b = np.reshape(b, (32, 32, 1))
                path_save = "BienSoXe_CatThanhTungKiTu/" +  path.split(".")[0] + "/" + str(i) + ".jpg"
                cv2.imwrite(path_save, b)
                i+=1
          
        for a in digitCnts:
            if(a[2] + 5 >= chieucao):
                b = imgCopy[a[2]:a[3],a[1]:a[0]]
                ret,b= cv2.threshold(b,127,255,cv2.THRESH_BINARY_INV)
                b = cv2.resize(b, (32, 32))
                b = np.reshape(b, (32, 32, 1))
                path_save = "BienSoXe_CatThanhTungKiTu/" +  path.split(".")[0] + "/" + str(i) + ".jpg"
                cv2.imwrite(path_save, b)
                i+=1
    
    
    
    
    
    
    
    
    
    
    
    
    